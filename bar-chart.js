new Chart(document.getElementById("bar-chart-horizontal"), {
    type: 'horizontalBar',
    data: {
      labels: ["Mi puntuación", "Media", "Total"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#75B633", "#FDC504","#9897A1"],
          data: [3678,3878,5267]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'RESULTADO GENERAL'
      }
    }
});

new Chart(document.getElementById("bar-chart-horizontal2"), {
    type: 'horizontalBar',
    data: {
      labels: ["Mi puntuación", "Media", "Total"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#75B633", "#FDC504","#9897A1"],
          data: [3678,3878,5267]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'RESULTADO EN PERSONAS'
      }
    }
});

new Chart(document.getElementById("bar-chart-horizontal3"), {
    type: 'horizontalBar',
    data: {
      labels: ["Mi puntuación", "Media", "Total"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#75B633", "#FDC504","#9897A1"],
          data: [3678,3878,5267]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'RESULTADO EN NEGOCIO'
      }
    }
});

new Chart(document.getElementById("bar-chart-horizontal4"), {
    type: 'horizontalBar',
    data: {
      labels: ["Mi puntuación", "Media", "Total"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#75B633", "#FDC504","#9897A1"],
          data: [3678,3878,5267]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'RESULTADO EN NEGOCIO'
      }
    }
});